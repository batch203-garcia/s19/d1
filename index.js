//console.log("Hello World!");

//[SECTION] if, else if, else statement

let numA = 0;

//if statement

if (numA < 0) {
    console.log("Hello");
}

//for checking the value
console.log(numA < 0);

let city = "New York";

if (city === "New York") {
    console.log("Welcome to New York!")
}

//else if Clause -> will be executed if previous condition is not met or the value is false -> it is optional

let numH = 0;

if (numH < 0) {
    console.log("Hello");
} else if (numH > 0) {
    console.log("world");
}


city = "Manila";

if (city === "New York") {
    console.log("Welcome to New York!")
} else if (city === "Tokyo") {
    console.log("Welcome to Tokyo, Japan!");
}

//else statement -> if all conditions are false -> optional also

if (numH < 0) {
    console.log("Hello");
} else if (numH > 0) {
    console.log("world");
} else {
    console.log("Again")
}

if (city === "New York") {
    console.log("Welcome to New York!")
} else if (city === "Tokyo") {
    console.log("Welcome to Tokyo, Japan!");
} else {
    console.log("City is not included in the list.");
}

//if, else if, and else statement inside a function

/*
    Scenario: We want to determine intensity of a typhone based on its wind speed.
    Not a Typoon - Wind speed is less than 30. 
    Tropical Depression - Wind speed is less than or equal to 61.
    Tropical Storm - Wind speed is between 62 to 88.
    Severe Tropical Storm - Wind speed is between 89 to 117.
    Typoon - Wind speed is greater than or equal to 118.

*/

let message = "No message";
console.log(message);

function determineTyphoonIntensity(windSpeed) {
    if (windSpeed < 30) {
        return "Not a typhoon";
    } else if (windSpeed <= 61) {
        return "Tropical Depression detected";
    } else if (windSpeed >= 61 && windSpeed <= 88) {
        return "Tropical Storm detected";
    } else if (windSpeed >= 89 && windSpeed <= 117) {
        return "Severe Tropical Storm detected";
    } else if (windSpeed >= 118) {
        return "Typoon detected";
    } else {
        return ""
    }
}

message = determineTyphoonIntensity(100);
console.log(message);

//console.warn() -> prints warning in console
if (message === "Severe Tropical Storm detected") {
    console.warn(message);
}

//[SECTION] Truthy and Falsy
// in JS a "truthy" value is considered true with boolean
//"falsy" value are exception to truthy -> false, 0, "", null, undefined, NaN

//Truthy example

if (true) {
    console.log("Truthy");
}

if (1) {
    console.log("Truthy");
}

if ([]) {
    console.log("Truthy");
}

//Falsy example

if (false) {
    console.log("Falsy");
}
if (0) {
    console.log("Falsy");
}
if (undefined) {
    console.log("Falsy");
}